package com.rebeca.nycschools.domain.model

import com.rebeca.nycschools.data.model.SatResultsModel

data class SatResult(
    var dbn: String? = null,
    var schoolName: String? = null,
    var numOfSatTestTakers: String? = "N/A",
    var satCriticalReadingAvgScore: String? = "N/A",
    var satMathAvgScore: String? = "N/A",
    var satWritingAvgScore: String? = "N/A"
)

fun SatResultsModel.toDomain(): SatResult {
    return SatResult(
        this.dbn,
        this.schoolName,
        this.numOfSatTestTakers ?: "",
        this.satCriticalReadingAvgScore ?: "",
        this.satMathAvgScore ?: "",
        this.satWritingAvgScore ?: ""
    )
}