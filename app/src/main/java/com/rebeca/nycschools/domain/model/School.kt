package com.rebeca.nycschools.domain.model

import com.rebeca.nycschools.data.model.SchoolModel

data class School(
    val dbn: String? = null,
    val schoolName: String? = null,
    val overviewParagraph: String? = null,
    val academicOpportunities: String? = null,
    val schoolSports: String? = null,
    val languageClasses: String? = null,
    val phoneNumber: String? = null,
    val schoolEmail: String? = null,
    val location: String? = null,
    val website: String? = null,
    val latitude: String? = null,
    val longitude: String? = null,
    val satResults: String? = null
)

fun SchoolModel.toDomain(): School {
    var academicOpportunities = academicOpportunities1 ?: ""
    academicOpportunities2?.let {
        academicOpportunities += "\n$it"
    }
    academicOpportunities3?.let {
        academicOpportunities += "\n$it"
    }
    academicOpportunities4?.let {
        academicOpportunities += "\n$it"
    }
    val locationAddress = "${primaryAddressLine1 ?: ""}, ${city ?: ""} ${zip ?: ""}"
    return School(
        dbn,
        schoolName ?: "",
        overviewParagraph ?: "",
        academicOpportunities,
        schoolSports,
        languageClasses ?: "",
        phoneNumber ?: "",
        schoolEmail ?: "",
        locationAddress,
        website ?: "",
        latitude,
        longitude
    )
}