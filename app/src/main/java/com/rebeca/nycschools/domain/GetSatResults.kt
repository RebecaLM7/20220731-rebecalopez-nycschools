package com.rebeca.nycschools.domain

import com.rebeca.nycschools.data.repositories.SatRepository
import com.rebeca.nycschools.domain.model.SatResult
import javax.inject.Inject

class GetSatResults @Inject constructor(private val repository: SatRepository) {

    suspend operator fun invoke(dbn: String): List<SatResult>? =
        repository.getSatResultsByDbn(dbn)
}