package com.rebeca.nycschools.domain

import com.rebeca.nycschools.data.repositories.SchoolsRepository
import com.rebeca.nycschools.domain.model.School
import javax.inject.Inject

class GetSchoolsDirectory @Inject constructor(private val repository: SchoolsRepository) {

    suspend operator fun invoke(): List<School>? = repository.getAllSchools()

}