package com.rebeca.nycschools.data.remote

import com.rebeca.nycschools.data.model.SatResultsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class SatResultsService @Inject constructor(private val api: SchoolsApi) {

    /* API call */
    suspend fun getSatResultsByDbn(dbn: String): List<SatResultsModel> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getSatResults(dbn)
                if (response.isSuccessful)
                    response.body() ?: emptyList()
                else
                    emptyList()
            } catch (e: Exception) {
                emptyList()
            }
        }
    }
}