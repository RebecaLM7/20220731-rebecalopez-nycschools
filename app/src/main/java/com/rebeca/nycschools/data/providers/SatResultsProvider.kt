package com.rebeca.nycschools.data.providers

import com.rebeca.nycschools.domain.model.SatResult
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SatResultsProvider @Inject constructor() {
    var satResults: List<SatResult> = emptyList()
}