package com.rebeca.nycschools.data.repositories

import com.rebeca.nycschools.data.providers.SatResultsProvider
import com.rebeca.nycschools.data.remote.SatResultsService
import com.rebeca.nycschools.domain.model.SatResult
import com.rebeca.nycschools.domain.model.toDomain
import javax.inject.Inject

class SatRepository @Inject constructor(
    private val api: SatResultsService,
    private val satResultsProvider: SatResultsProvider
) {

    suspend fun getSatResultsByDbn(dbn: String): List<SatResult> {
        /* API call */
        val response = api.getSatResultsByDbn(dbn)
        /* Save data locally and map ot to UI model */
        satResultsProvider.satResults = response.map { it.toDomain() }
        /* Return response */
        return response.map { it.toDomain() }
    }
}