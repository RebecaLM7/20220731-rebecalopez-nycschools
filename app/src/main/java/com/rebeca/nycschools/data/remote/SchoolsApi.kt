package com.rebeca.nycschools.data.remote

import com.rebeca.nycschools.BuildConfig
import com.rebeca.nycschools.data.model.SchoolModel
import com.rebeca.nycschools.data.model.SatResultsModel
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolsApi {
    @GET(BuildConfig.SCHOOLS_DIRECTORY_PATH)
    suspend fun getSchoolsDirectory(): Response<List<SchoolModel>>

    @GET(BuildConfig.SAT_RESULTS_PATH)
    suspend fun getSatResults(@Query("dbn") dbn: String?): Response<List<SatResultsModel>>
}