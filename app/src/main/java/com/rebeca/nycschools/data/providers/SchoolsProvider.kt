package com.rebeca.nycschools.data.providers

import com.rebeca.nycschools.domain.model.School
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class SchoolsProvider @Inject constructor() {
    var schools: List<School> = emptyList()
    var schoolSelected: School = School()
}