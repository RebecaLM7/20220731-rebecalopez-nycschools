package com.rebeca.nycschools.data.repositories

import com.rebeca.nycschools.data.providers.SchoolsProvider
import com.rebeca.nycschools.data.remote.SchoolDirectoryService
import com.rebeca.nycschools.domain.model.School
import com.rebeca.nycschools.domain.model.toDomain
import javax.inject.Inject

class SchoolsRepository @Inject constructor(
    private val api: SchoolDirectoryService,
    private val schoolsProvider: SchoolsProvider) {

    suspend fun getAllSchools(): List<School> {
        /* API call */
        val response = api.getSchoolsDirectory()
        /* Save data locally and map ot to UI model */
        schoolsProvider.schools = response.map { it.toDomain() }
        /* Return response */
        return response.map { it.toDomain() }
    }
}