package com.rebeca.nycschools.data.remote

import com.rebeca.nycschools.data.model.SchoolModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception
import javax.inject.Inject

class SchoolDirectoryService @Inject constructor(private val api: SchoolsApi){

    /* API call */
    suspend fun getSchoolsDirectory(): List<SchoolModel> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.getSchoolsDirectory()
                if (response.isSuccessful)
                    response.body() ?: emptyList()
                else
                    emptyList()
            } catch (e: Exception) {
                emptyList()
            }
        }
    }
}