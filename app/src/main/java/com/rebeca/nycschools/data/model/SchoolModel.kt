package com.rebeca.nycschools.data.model

import com.google.gson.annotations.SerializedName

data class SchoolModel(
    var dbn: String? = null,
    var neighborhood: String? = null,
    var location: String? = null,
    var website: String? = null,
    var city: String? = null,
    var zip: String? = null,
    var latitude: String? = null,
    var longitude: String? = null,
    @SerializedName("school_name") var schoolName: String? = null,
    @SerializedName("overview_paragraph") var overviewParagraph: String? = null,
    @SerializedName("academicopportunities1") var academicOpportunities1: String? = null,
    @SerializedName("academicopportunities2") var academicOpportunities2: String? = null,
    @SerializedName("academicopportunities3") var academicOpportunities3: String? = null,
    @SerializedName("academicopportunities4") var academicOpportunities4: String? = null,
    @SerializedName("ell_programs") var ellPrograms: String? = null,
    @SerializedName("language_classes") var languageClasses: String? = null,
    @SerializedName("phone_number") var phoneNumber: String? = null,
    @SerializedName("school_email") var schoolEmail: String? = null,
    @SerializedName("total_students") var totalStudents: String? = null,
    @SerializedName("extracurricular_activities") var extracurricularActivities: String? = null,
    @SerializedName("school_sports") var schoolSports: String? = null,
    @SerializedName("attendance_rate") var attendanceRate: String? = null,
    @SerializedName("primary_address_line_1") var primaryAddressLine1: String? = null,
    @SerializedName("state_code") var stateCode: String? = null,
    @SerializedName("community_board") var communityBoard: String? = null,
    @SerializedName("council_district") var councilDistrict: String? = null,
    @SerializedName("census_tract") var censusTract: String? = null
)