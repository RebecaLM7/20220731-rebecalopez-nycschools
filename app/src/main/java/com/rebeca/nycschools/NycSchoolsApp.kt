package com.rebeca.nycschools

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class NycSchoolsApp : Application()