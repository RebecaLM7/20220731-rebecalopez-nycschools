package com.rebeca.nycschools.ui.view

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.rebeca.nycschools.BR
import com.rebeca.nycschools.R
import com.rebeca.nycschools.databinding.FragmentSchoolDetailBinding
import com.rebeca.nycschools.domain.model.SatResult
import com.rebeca.nycschools.domain.model.School
import com.rebeca.nycschools.ui.listeners.ContactItemListener
import com.rebeca.nycschools.ui.viewmodel.SharedSchoolsViewModel

const val HTTP_URL_PREFIX = "http://"

class SchoolDetailFragment : Fragment(), ContactItemListener {

    private lateinit var sharedSchoolsViewModel: SharedSchoolsViewModel
    private lateinit var binding: FragmentSchoolDetailBinding
    private lateinit var dbn: String

    companion object {
        @JvmStatic
        fun newInstance() = SchoolDetailFragment()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        /* View Binding */
        binding = FragmentSchoolDetailBinding.inflate(layoutInflater)
        binding.listener = this

        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedSchoolsViewModel = activity?.run {
            ViewModelProvider(this)[SharedSchoolsViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        /* Initialize observers */
        initObservers()
    }

    private fun initObservers() {
        sharedSchoolsViewModel.schoolSelected.observe(this) {
            binding.setVariable(BR.model, it)
            dbn = it.dbn ?: ""
        }

        sharedSchoolsViewModel.satResultsModel.observe(this) {
            val satResult =
                if (!it.isNullOrEmpty() && !it[0].dbn.isNullOrEmpty() && dbn == it[0].dbn)
                    it[0]
                else
                    SatResult()
            binding.setVariable(BR.modelSat, satResult)

        }
    }

    override fun onContactItemListener(view: View, model: School) {
        when (view.tag.toString()) {
            "email" -> composeEmail(model.schoolEmail)
            "phone" -> dialPhoneNumber(model.phoneNumber)
            "website" -> openWebPage(model.website)
            "location" -> showMap(model.latitude, model.longitude, model.location)
            else -> return
        }
    }

    /**
     * Method to handle mail address intent
     * @param schoolEmail  email address to send
     */
    private fun composeEmail(schoolEmail: String?) {
        if (!schoolEmail.isNullOrEmpty() && android.util.Patterns.EMAIL_ADDRESS.matcher(schoolEmail)
                .matches()
        ) {
            val intent = Intent(Intent.ACTION_SENDTO).apply {
                data = Uri.parse("mailto:") // only email apps should handle this
                putExtra(Intent.EXTRA_EMAIL, arrayOf(schoolEmail))
            }
            sendIntent(intent)
        } else {
            showErrorToast()
        }
    }

    /**
     * Method to handle phone number intent
     * @param phoneNumber - phone number to be sent
     */
    private fun dialPhoneNumber(phoneNumber: String?) {
        if (phoneNumber.isNullOrEmpty())
            return
        if (android.util.Patterns.PHONE.matcher(phoneNumber).matches()) {
            val intent = Intent(Intent.ACTION_DIAL).apply {
                data = Uri.parse("tel:$phoneNumber")
            }
            sendIntent(intent)
        } else {
            showErrorToast()
        }
    }

    /**
     * Method to handle web site url intent
     * @param website - url to be sent
     */
    private fun openWebPage(website: String?) {
        if (!website.isNullOrEmpty()) {
            val url = if (!website.startsWith(HTTP_URL_PREFIX) && !website.startsWith("https://"))
                "$HTTP_URL_PREFIX$website" else website
            val webpage: Uri = Uri.parse(url)
            val intent = Intent(Intent.ACTION_VIEW, webpage)
            sendIntent(intent)
        }
    }

    /**
     * Method to handle location intent to open in a map
     * @param lat - latitude of the poi
     * @param long - longitude of the poi
     * @param location - address to be sent
     */
    private fun showMap(lat: String?, long: String?, location: String?) {
        if (!lat.isNullOrEmpty() && !long.isNullOrEmpty()) {
            val geoLocation = Uri.parse("geo:$lat,$long?q=" + Uri.encode(location ?: ""))
            val intent = Intent(Intent.ACTION_VIEW, geoLocation)
            sendIntent(intent)
        }
    }

    /**
     * Method to send and Intent and verify if any suitable application can handle it
     */
    private fun sendIntent(intent: Intent) {
        val manager = activity?.packageManager
        if (manager?.resolveActivity(intent, PackageManager.MATCH_ALL) != null) {
            startActivity(intent)
        } else {
            showErrorToast()
        }
    }

    /**
     * Method to show an error generic toast
     */
    private fun showErrorToast() {
        Toast.makeText(this.context, getString(R.string.action_not_available), Toast.LENGTH_SHORT)
            .show()
    }
}