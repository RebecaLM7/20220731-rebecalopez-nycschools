package com.rebeca.nycschools.ui.listeners

import com.rebeca.nycschools.domain.model.School

interface SchoolItemListener {
    fun onSchoolClicked(model: School)
}