package com.rebeca.nycschools.ui.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.fragment.NavHostFragment
import com.rebeca.nycschools.R
import com.rebeca.nycschools.databinding.ActivityMainBinding
import com.rebeca.nycschools.ui.viewmodel.SharedSchoolsViewModel
import dagger.hilt.android.AndroidEntryPoint

/**
 * Main Activity for schools list
 */
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val sharedSchoolsViewModel: SharedSchoolsViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        /* View Binding */
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        /* Initialize observers */
        initObservers()
    }

    private fun initObservers() {
        /* Observer to detect item clicked */
        sharedSchoolsViewModel.schoolSelected.observe(this) {
            sharedSchoolsViewModel.getSatResultsByDbn(it.dbn)
            val navHostFragment =
                supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
            val navController = navHostFragment.navController
            val action =
                SchoolsListFragmentDirections.actionSchoolsListFragmentToSchoolsDetailFragment()
            navController.navigate(action)
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()
        this.supportFragmentManager.popBackStack()
    }
}