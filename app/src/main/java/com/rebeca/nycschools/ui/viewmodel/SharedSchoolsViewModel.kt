package com.rebeca.nycschools.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.rebeca.nycschools.data.providers.SatResultsProvider
import com.rebeca.nycschools.data.providers.SchoolsProvider
import com.rebeca.nycschools.domain.GetSatResults
import com.rebeca.nycschools.domain.GetSchoolsDirectory
import com.rebeca.nycschools.domain.model.SatResult
import com.rebeca.nycschools.domain.model.School
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SharedSchoolsViewModel @Inject constructor(
    private val getSchoolsDirectory: GetSchoolsDirectory,
    private val getSatResults: GetSatResults,
    private val schoolsProvider: SchoolsProvider,
    private val satResultsProvider: SatResultsProvider
) : ViewModel() {
    val schoolsModel = MutableLiveData<List<School>>()
    val schoolSelected = MutableLiveData<School>()
    val satResultsModel = MutableLiveData<List<SatResult>>()

    var isLoading = MutableLiveData<Boolean>()

    init {
        fetchSchoolsList()
    }

    /* Fetch schools data */
    private fun fetchSchoolsList() {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getSchoolsDirectory()
            if (!result.isNullOrEmpty()) {
                schoolsModel.postValue(schoolsProvider.schools)
            }
            isLoading.postValue(false)
        }
    }

    /* Get list of schools stored*/
    fun getSchoolsList() {
        schoolsModel.postValue(schoolsProvider.schools)
    }

    /* Fetch schools data */
    fun getSatResultsByDbn(dbn: String?) {
        viewModelScope.launch {
            if (dbn.isNullOrEmpty())
                satResultsModel.postValue(satResultsProvider.satResults)
            else {
                isLoading.postValue(true)
                getSatResults(dbn)
                satResultsModel.postValue(satResultsProvider.satResults)
                isLoading.postValue(false)
            }
        }
    }

    /* Save the school selected when user clicks on item*/
    fun selectedSchool(item: School) {
        schoolsProvider.schoolSelected = item
        schoolSelected.postValue(schoolsProvider.schoolSelected)
    }
}