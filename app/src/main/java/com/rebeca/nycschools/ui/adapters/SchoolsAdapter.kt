package com.rebeca.nycschools.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.rebeca.nycschools.BR
import com.rebeca.nycschools.R
import com.rebeca.nycschools.databinding.SchoolItemBinding
import com.rebeca.nycschools.domain.model.School
import com.rebeca.nycschools.ui.listeners.SchoolItemListener

class SchoolsAdapter(private val listener: SchoolItemListener) :
    ListAdapter<School, SchoolsAdapter.SchoolsViewHolder>(DiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolsViewHolder {
        val binding = DataBindingUtil.inflate<SchoolItemBinding>(
            LayoutInflater.from(parent.context), R.layout.school_item, parent, false
        )
        return SchoolsViewHolder(binding, listener)
    }

    override fun onBindViewHolder(holder: SchoolsViewHolder, position: Int) {
        holder.bind(currentList[position])
    }

    override fun getItemCount(): Int = currentList.size

    class DiffCallback : DiffUtil.ItemCallback<School>() {

        override fun areItemsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem.dbn == newItem.dbn
        }

        override fun areContentsTheSame(oldItem: School, newItem: School): Boolean {
            return oldItem == newItem
        }

    }

    class SchoolsViewHolder(
        private val view: SchoolItemBinding,
        private val schoolListener: SchoolItemListener
    ) :
        RecyclerView.ViewHolder(view.root) {
        fun bind(model: School) {
            view.setVariable(BR.model, model)
            view.listener = schoolListener
            view.executePendingBindings()
        }

    }
}