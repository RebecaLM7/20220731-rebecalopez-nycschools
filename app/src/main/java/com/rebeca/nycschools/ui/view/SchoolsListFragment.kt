package com.rebeca.nycschools.ui.view

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.rebeca.nycschools.databinding.FragmentSchoolsListBinding
import com.rebeca.nycschools.domain.model.School
import com.rebeca.nycschools.ui.adapters.SchoolsAdapter
import com.rebeca.nycschools.ui.listeners.SchoolItemListener
import com.rebeca.nycschools.ui.viewmodel.SharedSchoolsViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.lang.Exception

@AndroidEntryPoint
class SchoolsListFragment : Fragment(), SchoolItemListener {

    companion object {
        fun newInstance() = SchoolsListFragment()
    }

    private lateinit var sharedSchoolsViewModel: SharedSchoolsViewModel
    private lateinit var binding: FragmentSchoolsListBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolsListBinding.inflate(layoutInflater)
        // Initialize RecyclerView adapter
        initAdapter()
        return binding.root
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        sharedSchoolsViewModel = activity?.run {
            ViewModelProvider(this)[SharedSchoolsViewModel::class.java]
        } ?: throw Exception("Invalid Activity")

        // Initialize observers
        initObservers()

    }

    private fun initAdapter() {
        binding.rvSchools.layoutManager = LinearLayoutManager(this.context)
        binding.rvSchools.adapter = SchoolsAdapter(this)
        sharedSchoolsViewModel.getSchoolsList()
    }

    private fun initObservers() {
        // Set observer for changes in the view model
        sharedSchoolsViewModel.schoolsModel.observe(this) { result ->
            if (result.isNotEmpty()) {
                (binding.rvSchools.adapter as SchoolsAdapter).submitList(result)
            }
        }

        // Additional observer to control the progress bar
        sharedSchoolsViewModel.isLoading.observe(this) {
            binding.progressCircular.isVisible = it
        }
    }

    override fun onSchoolClicked(model: School) {
        // Update selected item in shared ModelView
        sharedSchoolsViewModel.selectedSchool(model)
    }
}