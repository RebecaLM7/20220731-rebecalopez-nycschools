package com.rebeca.nycschools.ui.listeners

import android.view.View
import com.rebeca.nycschools.domain.model.School

interface ContactItemListener {
    fun onContactItemListener(view: View, model: School)
}