package com.rebeca.nycschools.data.remote

import com.rebeca.nycschools.data.model.SatResultsModel
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.impl.annotations.MockK
import kotlinx.coroutines.runBlocking
import okhttp3.ResponseBody.Companion.toResponseBody
import org.junit.Before
import org.junit.Test
import retrofit2.Response


class SatResultsServiceTest {

    @MockK
    private lateinit var api: SchoolsApi

    private lateinit var satResultsService: SatResultsService

    @Before
    fun onBefore(){
        MockKAnnotations.init(this)
        satResultsService = SatResultsService(api)
    }

    @Test
    fun whenApiFailsReturnEmptyList() = runBlocking {
        /* Given */
        coEvery { api.getSatResults("") } returns Response.error(404, "error".toResponseBody())

        /* When */
       val result = satResultsService.getSatResultsByDbn("")

        /* Then */
        assert(result.isEmpty())
    }

    @Test
    fun whenApiSuccessReturnValidList() = runBlocking {
        val satList = listOf(SatResultsModel("dbn", "school"))
        /* Given */
        coEvery { api.getSatResults("") } returns Response.success(satList)

        /* When */
        val result = satResultsService.getSatResultsByDbn("")
        /* Then */

        assert(result == satList)
    }
}